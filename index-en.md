# Mask straps

> Version française [ici](index-fr.md)

These mask attachment straps improve the tightness of the masks and relieve the ears when worn for long periods of time.  They are shared under a [Creative Commons license BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.en).

We have validated this model inspired from [thingiverse](https://www.thingiverse.com/thing:4249113) and [creality](https://www.creality.com/info/with-togetherness-and-cooperation-fight-against-the-novel-coronavirus-epidemic-i00282i1.html) by the Erasmus hospital in Brussels.

![](images/mask-strap.png)

The dimensions of the room are 190x20mm. The developed model can be laser cut. It has only 3 teeth to limit manufacturing time, and its size has been optimized to minimize material loss. 


## Material

* Material
  * polypropylene 0.8mm or similar
* Tools
  * Laser cutter
* Production time : <20s/piece

##  Cutting the piece

Download the SVG files below (optimized for an 800x500mm plate) and cut them with a laser cutter.

* [DXF 1 piece](files/mask-strap-19mm-3teeth.DXF)
* [SVG 90 pieces (800x500mm)](files/mask-strap-190-20mm-3teeth-90pc.svg)

![](/files/mask-strap-190-20mm-3teeth-90pc.svg)


## Authors and Acknowledgements

Tutorial directed by Orianne Bastin and Max Thulliez. Thanks to all the team for your contribution and your pictures.