# Solution à la découpeuse laser

> English version [here](index-en.md)

Ces bandeaux d'attache de masques permettent d'améliorer l'étanchéité des masques et de soulager les oreilles en cas de port prolongé.  Ils sont partagés sous une [licence Creative Commons BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr).

Nous avons fait valider ce modèle développé à partir de [thingiverse](https://www.thingiverse.com/thing:4249113) et de [creality](https://www.creality.com/info/with-togetherness-and-cooperation-fight-against-the-novel-coronavirus-epidemic-i00282i1.html) par l'hôpital Erasme à Bruxelles.

![](images/mask-strap.png)

Les dimensions de la pièce sont de 190x20mm. Le modèle développé peut être découpé au laser. Il ne comporte que 3 dents pour limiter le temps de fabrication, et sa taille a été optimisée pour minimiser les pertes de matériau. 

## Matériel

* Matériel
  * polypropylene 0.8mm ou similaire
* Outils :
  * découpeuse laser
* Temps de production : <20s/pièce

##  Découpe de la structure

Téléchargez les fichiers SVG ci-dessous (optimisés pour une plaque 800x500mm) et découpez-les à la découpeuse laser.

* [DXF 1 pièce](files/mask-strap-19mm-3teeth.DXF)
* [SVG 90 pièces (800x500mm)](files/mask-strap-190-20mm-3teeth-90pc.svg)

![](files/mask-strap-190-20mm-3teeth-90pc.svg)


## Auteurs et remerciements

Tutoriel réalisé par Orianne Bastin et Max Thulliez. Merci à toute l'équipe pour votre contribution et vos photos.